To use this, you need requests and datetime

To install on Windows:
    Open CMD and type "pip install requests" and "pip install DateTime"

To install on Linux:
    Open terminal and type "sudo python -m pip install requests" and
    "sudo python -m pip install DateTime"
    
To use this script you need to update:
    `'''dynamic_dns('subdomain.example.com', 'username', 'password')
    write_file('subdomain.example.com')
    dynamic_dns('subdomain.example.com', 'username', 'password')
    append_file('subdomain.example.com')'''`
by removing the ''' at the beginning and end. If you only have one Dynamic
DNS record to update you may remove the third and fourth line.